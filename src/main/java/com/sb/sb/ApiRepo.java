package com.sb.sb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiRepo {

    @Autowired
    PersonRepo personRepo;

    @RequestMapping(path = "/deletePerson", method = RequestMethod.DELETE)
    public void deletePerson(@RequestParam("name") String name, @RequestParam("surname") String surname, @RequestParam("age") int age) {

        List<Person> byNameAndSurnameAndAge = personRepo.findByNameAndSurnameAndAge(name, surname, age);
        personRepo.deleteAll(byNameAndSurnameAndAge);
    }

    @RequestMapping(path = "/postPerson", method = RequestMethod.POST)
    public void postPerson(@RequestParam("name") String name,
                           @RequestParam("surname") String surname,
                           @RequestParam("age") int age,
                           @RequestParam("newName") String newName,
                           @RequestParam("newSurname") String newSurname,
                           @RequestParam("newAge") int newAge) {

        List<Person> byNameAndSurnameAndAge = personRepo.findByNameAndSurnameAndAge(name, surname, age);
        Person person = byNameAndSurnameAndAge.get(0);
        person.setName(newName);
        person.setSurname(newSurname);
        person.setAge(newAge);
        personRepo.saveAndFlush(person);
    }

    @RequestMapping(path = "/addPerson", method = RequestMethod.PUT)
    public String addPerson(@RequestParam("name") String name,
                            @RequestParam("surname") String surname,
                            @RequestParam("age") int age){
        Person person = new Person();
        person.setName(name);
        person.setSurname(surname);
        person.setAge(age);

        personRepo.saveAndFlush(person);
        return "add";
    }

    @RequestMapping(path = "/getPerson", method = RequestMethod.GET)
    public List<Person>addPerson(){
        return personRepo.findAll();
    }
}
