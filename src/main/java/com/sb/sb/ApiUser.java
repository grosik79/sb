package com.sb.sb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class ApiUser {


    private UserService userService;

    @Autowired
    public ApiUser(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<User> getUserList() {
        return userService.getUserList();
    }
    @RequestMapping(method = RequestMethod.PUT)
       public void putUserList(@RequestParam("name") String name,@RequestParam("surname") String surname, @RequestParam("age") int age) {
       userService.addUser(name,surname,age);
    }

}
