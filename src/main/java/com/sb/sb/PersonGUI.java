package com.sb.sb;

import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

@SpringUI(path = "/gui")
public class PersonGUI extends UI {
ApiRepo apiRepo;

    @Autowired
    public PersonGUI(ApiRepo apiRepo) {
        this.apiRepo = apiRepo;
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        VerticalLayout verticalLayout = new VerticalLayout();
        Grid<Person> grid = new Grid<>();
        grid.addColumn(Person::getName).setCaption("Name");
        verticalLayout.addComponent(grid);
        grid.addColumn(Person::getName).setCaption("Surname");
        grid.addColumn(Person::getAge).setCaption("Age");

        Collection<Person> people = apiRepo.addPerson();
        ListDataProvider<Person> dataProvider = DataProvider.ofCollection(people);
        grid.setDataProvider(dataProvider);
        setContent(verticalLayout);


//        TextField filterTextField = new TextField("Filter by name");
//        filterTextField.setPlaceholder("name filter");
//        filterTextField.addValueChangeListener(event -> {
//            dataProvider.setFilter(Person::getName, name -> {
//                String nameLower = name == null ? ""
//                        : name.toLowerCase(Locale.ENGLISH);
//                String filterLower = event.getValue()
//                        .toLowerCase(Locale.ENGLISH);
//                return nameLower.contains(filterLower);
//            });
//        });
    }

}
