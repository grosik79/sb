package com.sb.sb;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepo extends JpaRepository<Person, Long> {
    List<Person> findByNameAndSurnameAndAge(String name, String surname, int age);

//@Query(value = "SELECT * FROM Person ")
//    List<Person> postPerson(String  name, String surname, int age);
//
//
//    @Query(value = "SELECT * FROM Person person WHERE person.id > :id", nativeQuery = true)
//    List<Person> findIds(Long id);
}
