package com.sb.sb;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private List<User> userList;

    public UserService() {
        userList = new ArrayList<>();
        userList.add(new User("Jan", "Kazik", 20));
        userList.add(new User("Jnusz", "Jeż", 25));
        userList.add(new User("Piotr", "Leń", 26));
        userList.add(new User("Jan", "Biały", 27));
        userList.add(new User("Jan", "Ciemny", 22));
        userList.add(new User("Jan", "Ogryzek", 28));
    }

    public List<User> getUserList() {
        return userList;
    }
    public void addUser(String name, String surname, int age){
        userList.add(new User(name, surname, age));
    }
}
